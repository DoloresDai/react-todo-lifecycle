import React, {Component} from 'react';
import './todolist.less';

function ShowTodo(props) {
    if (!props.showTodoList) {
        return null;
    }

    return (
        <div className="todoList">
            <Add/>
        </div>
    );
}
const list1 = ['Little Title 1']
const list2 = ['Little Title 1','Little Title 2']
const list3 = ['Little Title 1','Little Title 2','Little Title 3']
function ShowList(props) {
    if (!props.showList) {
        return null;
    }

    return (
        <ul>
            {props.list.map((value) =>
                <List key={value}
                          value={value} />
            )}
        </ul>
    );

}

function List(props) {
    if (props.showList) {
        return null;
    }
    return (<li>{props.value}</li>)
}

class Add extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showList: true,
        };
        this.handleList = this.handleList.bind(this);
    }

    handleList() {
        this.setState(prevState => ({
            showList: !prevState.showList
        }));
    }

    render() {
        return (
            <div>
                <button onClick={this.handleList}>{this.state.showList}Add</button>
                <ShowList list={list1} showList={this.state.showList}/>
                <ShowList list={list2} showList={!this.state.showList}/>
            </div>
        );
    }
}

class TodoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showTodoList: true,
        };
        this.handleShow = this.handleShow.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
    }

    handleShow() {
        this.setState(prevState => ({
            showTodoList: !prevState.showTodoList
        }));
    }

    handleRefresh() {
        this.setState(
            {
                showTodoList: false,
            }
        );
    }


    render() {
        return (
            <div>
                <button onClick={this.handleShow}>{this.state.showTodoList ? 'Hide' : 'Show'}</button>
                <button onClick={this.handleRefresh}>{this.state.showTodoList}Refresh</button>
                <ShowTodo showTodoList={this.state.showTodoList}/>
            </div>
        );
    }
}

export default TodoList;

