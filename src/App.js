import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component{
    constructor(props){
        super(props);
        this.state={
            showTodoList:true,
        }

    }
    render() {
        return (
            <div className='App'>
                <TodoList/>
            </div>
        );
    }
}

export default App;
